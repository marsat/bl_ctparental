#!/bin/bash
tar -xvzf blacklists2.tar.gz
mkdir blacklists/pgl_yoyo_org 
{
wget "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=1&mimetype=plaintext" -O - 2>/dev/null | grep -v "^#" | awk '{print $2}' | while read -r domaine
do
echo $domaine
#if [ "$(echo $domaine | grep -c "\.google\.com$" )" -eq "0" ];then
#	dig +short A $domaine 2>/dev/null | grep -E "^(((2[0-5][0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2}).){3}(2[0-5][0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2}))($|/(([1-9])|([1-2][0-9])|(3[0-2]))$)"
#	dig -6 +short AAAA $domaine 2>/dev/null |  grep -E "^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(([0-9A-Fa-f]{1,4}:){0,5}:((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(::([0-9A-Fa-f]{1,4}:){0,5}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))($|/(([1-9])|([1-9][0-9])|(1[0-1][0-9])|(12[0-8]))$)"
#fi
done
} > blacklists/pgl_yoyo_org/domains2
cat blacklists/pgl_yoyo_org/domains2 | sort -u | grep -vE  "^(.{2}--)|\..{2}--|^(.*-\.)|^(-)|^(#)|[+äâëêïîöôüûàçèé&\!@,;]|^$|.*blogspot\..*|\"" > blacklists/pgl_yoyo_org/domains
rm blacklists/pgl_yoyo_org/domains2
rm blacklists2.tar.gz
tar -cvzf blacklists2.tar.gz blacklists 
md5sum blacklists2.tar.gz > MD5SUM.LST

rm -rf blacklists
